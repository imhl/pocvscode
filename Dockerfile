FROM ubuntu

MAINTAINER honglie hltoh@suss.edu.sg

COPY sources.list /etc/apt

COPY code-server-3.1.1-linux-x86_64.tar.gz .

RUN tar -xzvf code-server-3.1.1-linux-x86_64.tar.gz && chmod +x code-server-3.1.1-linux-x86_64/code-server

RUN rm -rf code-server-3.1.1-linux-x86_64.tar.gz

RUN useradd -m student
RUN usermod -aG sudo student

ADD studentproj/ /home/student/

RUN apt-get update && apt-get install python3 python3-pip sudo wget curl -y
RUN ln -s /usr/bin/pip3 /usr/bin/pip && ln -s /usr/bin/python3 /usr/bin/python
COPY docker-entrypoint.sh /usr/local/bin/

COPY settings.json /home/student/.local/share/code-server/User/
RUN chown -R student:student /home/student/.local/

USER student
WORKDIR /home/student/studentproj

ENTRYPOINT ["docker-entrypoint.sh"]
