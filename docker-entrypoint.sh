#!/bin/bash

if [ $# -eq 0 ]
  then
    cd /home/student/studentproj
    /code-server-3.1.1-linux-x86_64/code-server --disable-ssh --host=0.0.0.0  --auth=none .
  else
    exec "$@"
fi
